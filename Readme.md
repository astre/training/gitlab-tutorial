# Git + GitLab Tutorial

This tutorial is structured as a series of _missions_ or _challenges_. Each one should be easy to accomplish. You will progressively unlock the skills needed to use `git` at a basic level for collaborating on GitLab. 

I will provide links to necessary materials and to further information.

## __Mission 1__ Get started with `git` locally

__Objective:__ You will be able to version-control your own work.


1. Install `git` on your computer

    `git` is a program for managing versions of code (or text).
    Install it and set it up.

    https://happygitwithr.com/install-git.html

    https://happygitwithr.com/hello-git.html

    We will also borrow some useful settings from https://swcarpentry.github.io/git-novice/02-setup.html, such as the automatic handling of line-endings.


2. Install a `git` _client_ (optional)

This is a program that interfaces `git` and provides a graphpical user interface, similar to RStudio interfacing `R`.
There are plenty of options both commercial and free, depending on the operating system.

I will use barebones `git` in the tutorial to cover the basics. But you are welcome to try some client.
RStudio provides very basic functionality. I've found [Gittyup](https://murmele.github.io/Gittyup/) a nice, free, cross-platform alternative.

https://happygitwithr.com/git-client.html


3. Set up a working directory for the tutorial

- Create a new directory called `gitlab_test`.
- Create a plain text file named `src/script.R` with a few lines of code within the subdirectory `src/`.
- Create a plain text file named `Readme.md` and write a title and some description of this test project.
- Create a Word document named `doc/binary.docx` and write a few words.


4. Set up a `git` local repository in your working directory

Open a terminal and change to your working directory.
Run `git init` in order to set up a `git` local repository and start tracking versions. 
Running `git status` will inform you that your files are untracked. 

5. Commit the first version of your work

You have to explicitly tell `git` which files you want to track.
For the moment, we will track __everything__: `git add .`
We will create the first _version_ (think of a snapshot of your work at this time) with `git commit -m "Initial commit"`.

What commes after `-m` is a label known as _commit message_ that informs about the purpose of the changes.

Running `git status` now should say that nothing has changed in your working directory since the last version.


6. Make and explore changes

Go make some changes in both the `script.R` and `binary.docx`.
Try `git diff`. 
Git will tell you which files have changed since last version,
and what the changes were... as far as it can read (i.e. not
in `binary.docx`)



__Deliverable:__ The output of `git diff`


__Summary:__ You have set up git in your local computer and learnt how to set up new repositories with `git init`, track new files with `git add`, make new versions with `git commit`, check the state of the repository with `git status` and see the latest changes with `git diff`.

You are now ready to version-control your own work in your computer.

Congratulations!! you have completed your first mission towards learning how to collaborate with `git` and GitLab.
Send me the deliverable by e-mail and you will be able to continue with your second mission.


## __Mission 2__ Get started with GitLab

__Objective:__ You will be able to share your work with colleagues.


1. Ceate an account on Forgemia GitLab instance using your credentials from Cirad.

Go to https://forgemia.inra.fr/ and click on `Sign in > Renater identity federation`. Select `Cirad`` on the list of institutions and log in with your credentials.

That's it! the first time you do this, GitLab creates your account.

Go ahead and explore a bit. Configure your settings and set up your profile. In particular, consider changing your username at `User Settings > Account`. This name will be part of the URL of your personal repositories and webpages. 


2. Establish secure communication between `git` and GitLab.

This is very OS-dependent. But here are detailed instructions for every platform.

https://forgemia.inra.fr/help/user/ssh.md

Also here:

https://docs.gitlab.com/ee/user/ssh.html#generate-an-ssh-key-pair

This can also be done conveniently from within RStudio, as explained here:

https://happygitwithr.com/ssh-keys.html


Let me know which one worked best for you.

These two steps are only required __once__ for each computer you use.


3. Set up a new project in GitLab as a remote repository of your local one

Search for "New project" in the top bar (symbol "+"). Name it "GitLab test", and place it under your personal namespace.
Establish the visibility as "Private". Leave everything else as is.

In particular __do not__ initialize it with a README file. This is for a case where
you start your project here. We already have a local repository.


4. Link your local and remote repositories

Go back to your command line and type the following:

```
git remote add origin git@forgemia.inra.fr:<your_username>/gitlab-test.git
git push -u origin --all
```

This adds a __remote repository__ named _origin_ at the given URL (there can be multiple remotes).
The second line _pushes_ all your local history to the remote repository creating permanent links between the corresponding branches. So far, you have a single _branch_, named _main_. But you could create more, where you can develop things in parallel without breaking anything in the _main_ branch.

If you refresh your web browser, you should see your files and the contents of your `Readme.md` file rendered as a welcome page.

This step is only needed once, when you create your project in GitLab.


5. Push new changes to the remote repository

If you look closely, your files in GitLab are not identical to those in your working directory. The last local changes have not been yet commited. They exist only in your _Working space_

`git status` tells you which files have changed.

`git add .` marks all your changes for commit. You could want to commit only part of the changes, or changes to some of the files only.

`git commit -m "Changed the script and the doc."` will create a new version __in the local repository__.

`git push` will send the updates to GitLab.


6. Join the group ASTRE

Groups - Explore groups. Search ASTRE. Click. Request Access.

![](img/gitlab_access-request.png)

This will grant you visibility across the projects of the UMR ASTRE. Moreover, next time you create a project you can define it either as a "personal" project or as a project in the _namespace_ of the UMR ASTRE.


__Deliverable:__ Your request to join ASTRE.


__Summary:__ You have set up your GitLab account and created a remote repository linked to your local `git` repository. These are operations needed only at the start of a project. You commited new changes to the _local_ repository and then pushed them to the _remote_ repository. You joined the group ASTRE.

You are now ready to share your version-controled work with your colleagues.

Congratulations!! you have completed your second mission towards learning how to collaborate with `git` and GitLab.


## __Mission 3__ Integrating contributions to your work

__Objective:__ You will be able integrate work from your colleagues into a common project.

1. Add __me__ as a collaborator of your project

Search for Facundo Muñoz (@facundo.munoz) in the left bar and Invite me with a Maintainer role.

__Roles__, roughly speaking: 
- Guests: can __see__
- Reporters: + can also manage __issues__
- Developers: + can also __propose__ changes
- Maintainers: + can directly __push__ changes and also manage users/groups and edit project settings
- Only the Owner can remove the project.


2. Make some changes on your script, in your local working directory.

Commit your changes to your local repository. But don't push yet.

3. Give me some time to make some "work" from my side. Wait for a notification from me before proceeding.

4. Try to push your changes to GitLab.

Rejected! _The remote contains work that you do not have locally._
The error message says it all.


5. Integrate the remote changes and solve potential __conflicts__

`git pull` 

The last message says that the _automatic_ merge failed, because there has been some CONFLICTS that need to be fixed.

If you `git status`, you will see a new file LICENSE and a modified file Readme.md that are ready to be commited. Those are changes that I have done and that have been automatically integrated.

Below, the conflicting files are shown. These are changes from your side and my side that `git` didn't know how to integrate, because we both changed the same parts.

The doc/binary.docx is unreadable by `git`. Thus, it only knows that it has changed at both sides. You need to manually solve the conflict and save a new version of the file.

On the other hand, open `src/script.R` in a text editor. You will see some markup (lines starting with `<<<<`; `|||||`; `======` and `>>>>`) that indicate the conflicting parts of the file. Integrate both changes manually and make sure to remove the markup.

Mark all conflicts as __resolved__ with `git add .` Check that everything is ready with `git status`, and then commit and push changes.
This is known as a _merge commit_ because its purpose is to integrate changes from both sources.

---

[Git+GitLab Crash Course](https://forgemia.inra.fr/umr-astre/training/forgemia-tutorial) by Facundo Muñoz is [licensed](LICENCE) under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/?ref=chooser-v1) 

<img src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1" width="22">
<img src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1" width="22">
<img src="https://mirrors.creativecommons.org/presskit/icons/sa.svg?ref=chooser-v1" width="22">

